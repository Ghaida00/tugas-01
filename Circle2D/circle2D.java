package Circle2D;
public class circle2D {
    //Data Fields
    private double x;
    private double y;
    private double radius;

    // No-arg constructor
    public circle2D() {
        this(0,0, 1);
    }
    //constructor with specified x, y, & radius
    circle2D(double x, double y, double radius){
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    //Getter for x
    public double getX (){
        return x;
    }
    //Getter for y
    public double getY (){
        return y;
    }
    //Getter for radius
    public double getRadius(){
        return radius;
    }
    //Method to return the area of the circle
    public double getArea(){
        return Math.PI* Math.pow(radius, 2);
    }
    //Method to returns the perimeter of the circle
    public double getPerimeter(){
        return 2* Math.PI*radius;
    }
    //Method to returns true if the specified circle is inside the specified point (x,y) is inside the circle
    public boolean contains(double x, double y) {
        double distanceFromCenter = Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
        return distanceFromCenter <= radius;
    }

    // Method to returns true if the specified circle is inside this circle.
    public boolean contains(circle2D circle) {
        double distanceBetweenCenters = Math.sqrt(Math.pow(circle.x - this.x, 2) + Math.pow(circle.y - this.y, 2));
        return distanceBetweenCenters + circle.radius <= radius;
    }

    // Method to returns true if the specified circle overlaps with this circle
    public boolean overlaps(circle2D circle) {
        double distanceBetweenCenters = Math.sqrt(Math.pow(circle.x - this.x, 2) + Math.pow(circle.y - this.y, 2));
        return distanceBetweenCenters <= radius + circle.radius;
    }
} 
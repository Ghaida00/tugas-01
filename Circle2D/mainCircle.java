package Circle2D;
public class mainCircle {

    public static void main(String[] args) {
        circle2D c1 = new circle2D(2, 2, 5.5);

        //Display the area
        System.out.println("Show Area of c1: " + c1.getArea());
        //Display the perimeter
        System.out.println("Show Perimeter of c1: " + c1.getPerimeter());
        //Display the Result of c1.contains(3,3)
        System.out.println("Result of c1.contains(3,3): " + c1.contains(3, 3));
        //Display the Result of c1.contains(new Circle2D(4, 5, 10.5))
        System.out.println("Result of c1.contains(new Circle2D(4, 5, 10.5)): " + c1.contains(new circle2D(4, 5, 10.5)));
        //Display the Result of c1.overlaps(new Circle2D(3, 5, 2.3))
        System.out.println("Result of c1.overlaps(new Circle2D(3, 5, 2.3)): " + c1.overlaps(new circle2D(3, 5, 2.3)));
    }
}